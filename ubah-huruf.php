<?php
function ubah_huruf($string)
{
    $letters = range('a', 'z');
    $temp = '';
    foreach (str_split($string) as $k => $v) {
        $key = array_search($v, $letters);
        $temp .= $letters[$key + 1];
    }
    return $temp . "<br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
?>